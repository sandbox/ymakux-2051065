(function ($) {
  Drupal.behaviors.autocompleteApi = {
    attach: function (context, settings) {
      $.each(Drupal.settings.autocomplete_api, function(i, v){
      
        var searchingMessage = "<div class='searching-status'>" + Drupal.t("Searching...") + "</div>";
        
        var input = $("input#" + i);
        var formId = input.closest("form").attr("id");
        input.autocomplete({
          search: function( event, ui) {
            $("div.searching-status").remove();
            $(this).after(searchingMessage);
          },
          source: function(request, response) {
            $.ajax({
              url: Drupal.settings.basePath + "autocomplete-api/source",
              dataType: "json",
              data: {
                term : request.term,
                inputId : input.attr("id"),
                inputRel : input.attr("rel"),
                formId : formId,
              },
              success: function(data) {
                response(data);
                if (data.length === 0) {
                  $("div.searching-status").remove();
                }
              }
            });
          },
          select: function(event, ui) {
            $("div.searching-status").remove();
            $.post(Drupal.settings.basePath + "autocomplete-api/select", {json: JSON.stringify({ui:ui,id:i,settings:v})});
          },
          close: function( event, ui ) {
            $("div.searching-status").remove();
          },
          open: function( event, ui ) {
            $("div.searching-status").remove();
          },
        });
      });
    }
  };
}(jQuery));